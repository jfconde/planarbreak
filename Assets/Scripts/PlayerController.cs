﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]

public class PlayerController : MonoBehaviour
{
    public GameObject destroyParticleSystem;
    public const float SPEED_SCALE_FACTOR = 7.5f;
    public const float AIM_MOVE_SLOWDOWN_PC = 0.25f;
    public const float MIN_SPEED_TO_APPLY_ROTATION = 0.1f;
    private Rigidbody2D rigidBody2D;
    public Player currentPlayer;
    private Animator animator;
    private bool isAiming = false;
    private bool controlDisabled = false;
    private float lastMove = 0f;

    private BattleEventDomain playerDomain;
    private BattleEventDomain enemyDomain;

    // Use this for initialization
    void Start()
    {
        // In debugging the order of scripts is altered so that GameData executes
        // before this script. In more complete builds, however, the GameData 
        // gameObject and class will have been initialized and persisted between
        // scenes.
        currentPlayer = GameDataManager.GetCurrentManager().playerObj;
        if (!currentPlayer)
        {
            currentPlayer = gameObject.AddComponent<Player>();
        }
    }

    private void Awake()
    {

        playerDomain = BattleEventDomain.GetPlayerDomain();
        enemyDomain = BattleEventDomain.GetEnemyDomain();
        Camera.main.GetComponent<CameraScript>().target = gameObject;
        rigidBody2D = gameObject.GetComponent<Rigidbody2D>();
        animator = gameObject.GetComponent<Animator>();
        TrailRenderer tr = gameObject.GetComponent<TrailRenderer>();
        if (tr != null)
        {
            tr.sortingLayerName = "Player";
        }
    }

    // Update is called once per frame
    void Update()
    {
  
    }

    private void FixedUpdate()
    {
        if (currentPlayer == null)
        {
            currentPlayer = GameDataManager.GetCurrentManager().playerObj;
        }

        if (!controlDisabled)
        {
            ApplyMovement();
        }

              if (controlDisabled)
        {
            isAiming = false;
            return;
        }

        if (InputManager.instance.AimButtonDown())
        {
            Aim();
            isAiming = true;
        }
        else
        {
            isAiming = false;
        }
    }

    private void ApplyMovement()
    {
        if (rigidBody2D != null && currentPlayer != null)
        {
            float currentSpeed = InputManager.instance.GetCurrentSpeed();
            Vector2 currentDirection = InputManager.instance.GetCurrentDirection();
            rigidBody2D.velocity = CalculateVelocity(currentSpeed, currentDirection, isAiming);

            if (currentSpeed > MIN_SPEED_TO_APPLY_ROTATION && !isAiming)
            {
                ApplyMovementRotation(currentDirection);
            }
            animator.SetFloat("speedX", Mathf.Abs(rigidBody2D.velocity.x));
            animator.SetFloat("speedY", Mathf.Abs(rigidBody2D.velocity.y));
            currentPlayer.SetSpeed(rigidBody2D.velocity.magnitude);
        }
    }

    private Vector2 CalculateVelocity(float speed, Vector2 direction, bool isAiming) {
        return direction * speed * SPEED_SCALE_FACTOR * (isAiming ? (1.0f - AIM_MOVE_SLOWDOWN_PC) : 1.0f);
    }

    private void ApplyMovementRotation(Vector2 rotateTowards)
    {
        if (rigidBody2D != null)
        {
            rigidBody2D.freezeRotation = true;
        }
        transform.up = Vector2.Lerp(transform.up, rotateTowards, Time.deltaTime * 10f);
    }

    private void Aim()
    {
        Vector2 aimDirection = InputManager.instance.GetAimDirection(transform.position);

        // set vector of transform directly
        transform.up = Vector2.Lerp(transform.up, aimDirection, Time.deltaTime * 10f);
    }

    public void DisableControl()
    {
        controlDisabled = false;
        gameObject.GetComponent<TrailRenderer>().enabled = false;
    }

    public void EnableControl()
    {
        controlDisabled = false;
        gameObject.GetComponent<TrailRenderer>().enabled = true;
    }

    public void Die()
    {
        Instantiate(destroyParticleSystem.gameObject, new Vector3(86, -16, 0), Quaternion.identity);

    }

    public bool GetAiming()
    {
        return isAiming;
    }
}