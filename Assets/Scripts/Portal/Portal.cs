﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour {
	public string destinationLevel;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	/// <summary>
	/// Sent when another object enters a trigger collider attached to this
	/// object (2D physics only).
	/// </summary>
	/// <param name="other">The other Collider2D involved in this collision.</param>
	public void OnTriggerEnter2D(Collider2D other)
	{
		if (Obj.Null(destinationLevel) || destinationLevel == "") {
			Debug.LogWarning("W001 - Entered portal with no destinationLevel set.");
		}
		GameDataManager manager = GameDataManager.GetCurrentManager();
		if (manager) {
			manager.playerObj.addLevelAccess(destinationLevel);
			GameSaveManager.GetInstance().SaveGame(manager.playerData);
		}
		UnityEngine.SceneManagement.SceneManager.LoadScene("Scenes/MainMap/MainMap");
	}
}
