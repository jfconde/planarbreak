public static class Obj {
    public static bool Null <T> (T obj) {
        return obj == null;
    }

    public static bool NotNull <T> (T obj) {
        return obj != null;
    }
}