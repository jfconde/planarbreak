using UnityEngine;
using System.Collections;

[System.Serializable]
public class LevelGroup
{
    public string id;
    public string name;
    
    public string description;

    public int order;

    public bool locked;

    public string color;

    public LevelItem[] levels;
}