using UnityEngine;
using System.Collections;

[System.Serializable]
public class LevelItem
{
    public string groupId;
    public string id;
    public string name;

    public bool unlocked;
}