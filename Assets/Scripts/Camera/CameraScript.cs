﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {
    public GameObject target;
	float z;
	// Use this for initialization
	void Start () {
		z = transform.position.z;
	}

	// Update is called once per frame
	void LateUpdate () {
		if(target != null)
		{
			transform.position = target.transform.position - new Vector3(0, 0, -z);
		}
	}
}
