﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * Class in charge of controlling the movement of the main player, acting as
 * an intermediate layer to abstract from the underlying platform: either
 * keyboard+mouse, controller or tapping.
 */
public class InputManager : MonoBehaviour
{
	/* Autoexplicative - Is the current platform mobile or desktop? */
	public bool isMobile;
	public bool isDesktop;

	// touchId of the current finger that is being used for movement.
	private int moveTouchId = -1;
	// Original screen position of the movement finger.
	private Vector2 moveTouchOrigin;
	// Track the current speed/direction of the movement finger (only used in Mobile)
	private Vector2 currentSpeed = Vector2.zero;
	// touchId of the current finger that is being used for aiming
	private int aimTouchId = -1;
	// Original screen position of the movement finger.
	private Vector2 aimTouchOrigin;
	// Track the current direction of aiming.
	private Vector2 aimDirection;

    // The distance (in cm) for a movement touch respect to its origin that translates to max speed.
	public const float MOVE_RADIUS_CM = 1.0f;
	// Const based on aspect ratio (IIRC).
	public const float DPI_TO_CM = 2.54f;
	// Sprite to draw as a movepad (debugging, and ugly right now)
	public RawImage movePadSprite;
    // Current InputManager instance.
	public static InputManager instance;

	void Awake ()
	{
		instance = this;
	}

	void Start ()
	{
		isMobile = Input.touchSupported;
		isDesktop = Input.mousePresent;
		instance = this;
	}

	void Update ()
	{
		// Just process the input (no as easy as it sounds)
		ProcessTouchInput ();
	}

	public void ProcessTouchInput ()
	{
		int i;
		Touch currentTouch;

		// Iterate for every current touch.
		for (i = 0; i < Input.touches.Length; i++) {
			currentTouch = Input.touches [i];
			if (currentTouch.phase == TouchPhase.Began) {
				// Process each touch initially depending on the screen half.

				// If there is not already an existing moveTouchId and touch happened on the left half => MOVEMENT
				if (moveTouchId < 0 && currentTouch.position.x <= Screen.width / 2.0f) {
					ProcessNewMoveTouch (currentTouch);
				}
				//  If there is not already an existing aimTouchId and touch happened on the right half => AIM
				if (aimTouchId < 0 && currentTouch.position.x >= Screen.width / 2.0f) {
					ProcessNewAimTouch (currentTouch);
				}
			} else {
				// If the touch had already started, just check whether we are dealing with movement, aim or do nothing.
				if (moveTouchId == currentTouch.fingerId) {
					ProcessMoveTouch (currentTouch);
				}

				if (aimTouchId == currentTouch.fingerId) {
					ProcessAimTouch (currentTouch);
				}
			}

            
		}

	}

	public void ProcessMoveTouch (Touch currentTouch)
	{
		if (currentTouch.phase == TouchPhase.Ended || currentTouch.phase == TouchPhase.Canceled) {
			// If the touch ended, reset the id and hide the movePad.
			moveTouchId = -1;
			if (movePadSprite != null) {
				movePadSprite.color = new Color (0, 0, 0, 0);
			}
		} else {
			// Otherwise, save the current "speed" (direction and magnitude).
			currentSpeed = currentTouch.position - moveTouchOrigin;
			// And if we are using a movePad, show it and draw it in its position.
			if (movePadSprite != null) {
				movePadSprite.color = Color.white;
				movePadSprite.rectTransform.position = new Vector2 (moveTouchOrigin.x, moveTouchOrigin.y);
				Vector2 screenCM = GetScreenSizeCentimeters ();
				movePadSprite.rectTransform.sizeDelta = new Vector2 (2.0f * MOVE_RADIUS_CM / screenCM.x * Screen.width, 2.0f * MOVE_RADIUS_CM / screenCM.y * Screen.height); 
			}
		}
	}

	public void ProcessNewMoveTouch (Touch currentTouch)
	{
		moveTouchId = currentTouch.fingerId;
		moveTouchOrigin = currentTouch.position;
	}

	public void ProcessAimTouch (Touch currentTouch)
	{
		if (currentTouch.phase == TouchPhase.Ended || currentTouch.phase == TouchPhase.Canceled) {
			aimTouchId = -1;
		} else {
			aimDirection = currentTouch.position - aimTouchOrigin;
		}
	}

	public void ProcessNewAimTouch (Touch currentTouch)
	{
		aimTouchId = currentTouch.fingerId;
		aimTouchOrigin = currentTouch.position;
	}

	public Vector2 GetCurrentDirection ()
	{
		if (isMobile) {
			if (moveTouchId >= 0) {
				return currentSpeed.normalized;
			}
		} else if (isDesktop) {
			return new Vector2 (Input.GetAxis ("Horizontal"), Input.GetAxis ("Vertical")).normalized;
		}

		return Vector2.zero;

	}

	public float GetCurrentSpeed ()
	{
		if (isMobile) {
			Vector2 screenDPI = GetDPI ();
			Vector2 touchDistance = new Vector2 (currentSpeed.x / screenDPI.x * DPI_TO_CM, currentSpeed.y / screenDPI.y * DPI_TO_CM);

			return Mathf.Min (touchDistance.magnitude, MOVE_RADIUS_CM) / MOVE_RADIUS_CM;
		} else if (isDesktop) {
			return Mathf.Min (1.0f, new Vector2 (Input.GetAxis ("Horizontal"), Input.GetAxis ("Vertical")).magnitude);
		}
		return 0.0f;
	}

	public Vector2 GetDPI ()
	{
		AndroidJavaClass activityClass = new AndroidJavaClass ("com.unity3d.player.UnityPlayer");
		AndroidJavaObject activity = activityClass.GetStatic<AndroidJavaObject> ("currentActivity");

		AndroidJavaObject metrics = new AndroidJavaObject ("android.util.DisplayMetrics");
		activity.Call<AndroidJavaObject> ("getWindowManager").Call<AndroidJavaObject> ("getDefaultDisplay").Call ("getMetrics", metrics);

		return new Vector2 (metrics.Get<float> ("xdpi"), metrics.Get<float> ("ydpi"));
	}

	public Vector2 GetScreenSizeCentimeters ()
	{
		Vector2 dpi = GetDPI ();
		Vector2 screenSize = new Vector2 (Screen.width / dpi.x * DPI_TO_CM, Screen.height / dpi.y * DPI_TO_CM);

		return screenSize;
	}

	public bool AimButtonDown ()
	{
		if (isMobile) {
			return aimTouchId >= 0;
		} else if (isDesktop) {
			return Input.GetMouseButton (1);
		}

		return false;
	}

	public Vector2 GetAimDirection (Vector2 playerTransformPosition)
	{
		if (isMobile) {
			return aimDirection;
		} else if (isDesktop) {
			// convert mouse position into world coordinates
			Vector2 mouseScreenPosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);

			// get direction you want to point at
			Vector2 direction = (mouseScreenPosition - playerTransformPosition).normalized;
			return direction;
		}

		return Vector2.zero;
	}
}
