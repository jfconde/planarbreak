using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class LevelManager
{

    private LevelGroup[] levelGroups;
    public LevelManager()
    {
        TextAsset jsonContent = Resources.Load("Map/LevelInformation") as TextAsset;
        LevelGroups groups = JsonUtility.FromJson<LevelGroups>(jsonContent.text);
        this.levelGroups = groups.groups;
    }

    public LevelGroup[] GetLevelGroups()
    {
        return levelGroups;
    }

    public LevelGroup GetLevelGroupById(string id) {
        foreach(LevelGroup group in levelGroups) {
            if (group.id == id) {
                return group;
            }
        }
        return null;
    }
}