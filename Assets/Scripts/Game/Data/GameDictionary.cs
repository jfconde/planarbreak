using System;
using System.Collections.Generic;
using UnityEngine;

public class GameDictionary : MonoBehaviour
{
    private List<Ship> ships = new List<Ship>();
    private List<Ability> abilities = new List<Ability>();
    private List<Upgrade> upgrades = new List<Upgrade>();

    void Start()
    {
        CreateData();
    }

    private void CreateData()
    {
        ships.Add(new Ship("dummy", "Dummy", "Description of Dummy", 3.0f, 0.4f, 0.0f, new Damage(1.0f), new Damage(0.2f)));
        ships.Add(new Ship("ship2", "Ship2", "Description of Ship2", 5.0f, 0.8f, 0.5f, new Damage(1.0f), new Damage(0.2f)));

        abilities.Add(new Ability("ab001", "Attack", "Something about attack", true, 1.5f, Damage.damageType.PHYS, null));

        upgrades.Add(new Upgrade("up001", "Upgrade1", "Description upgrade1", 1.0f, 0.0f, 0, 0.0f, new Damage(), new Damage(), new Damage(), new Damage()));
        upgrades.Add(new Upgrade("up002", "Upgrade2", "Description upgrade2", 0.0f, 0.0f, 0, 0.3f, new Damage(), new Damage(), new Damage(), new Damage()));
    }

    public List<Ship> GetShips()
    {
        return ships;
    }

    public List<Ability> GetAbilities()
    {
        return abilities;
    }

    public List<Upgrade> GetUpgrades()
    {
        return upgrades;
    }

    public Ship GetShip(string shipId)
    {
        foreach (Ship s in ships)
        {
            if (s.id == shipId)
            {
                return s;
            }
        }
        return null;
    }

    public Ability GetAbility(string abilityId)
    {
        foreach (Ability a in abilities)
        {
            if (a.id == abilityId)
            {
                return a;
            }
        }
        return null;
    }
    public Upgrade GetUpgrade(string upgradeId)
    {
        foreach (Upgrade u in upgrades)
        {
            if (u.id == upgradeId)
            {
                return u;
            }
        }
        return null;
    }
}