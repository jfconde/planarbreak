﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour {
	const string DEFAULT_SPAWN_NAME = "SPAWN_INITIAL";
	public GameObject gameDataPrefab;
	// Use this for initialization
	void Awake () {
		// Only for stand-alone testing
		GameDataManager manager = GameDataManager.GetCurrentManager ();
		if (manager) {
			manager.playerObj.RespawnValues ();
		} else {
			GameObject.Instantiate (gameDataPrefab);
			manager = GameDataManager.GetCurrentManager ();
		}
		GameSaveManager.GetInstance ();
		SpawnPlayer (manager.playerObj, manager.nextSpawnPoint != "" ? manager.nextSpawnPoint : Level.DEFAULT_SPAWN_NAME);
		manager.nextSpawnPoint = DEFAULT_SPAWN_NAME;
	}

	public void Reload () {
		Scene scene = SceneManager.GetActiveScene ();
		SceneManager.LoadScene (scene.name);
		Time.timeScale = 1.0f;
	}

	public void Quit () {
		SceneManager.LoadScene ("Scenes/MainMap/MainMap");
		Time.timeScale = 1.0f;
	}

	public void SpawnPlayer (Player player, string spawnName) {
		GameObject[] spawnPoints = GameObject.FindGameObjectsWithTag ("spawnPoint");
		GameObject spawnPoint = null;
		foreach (GameObject p in spawnPoints) {
			if (p.name == spawnName) {
				spawnPoint = p;
			}
		}

		if (Obj.NotNull (spawnPoint)) {
			// Do spawn the player
			GameObject Ship = Resources.Load ("Ships/Dummy/Ship") as GameObject;
			GameObject newPlayer = Instantiate (Ship, spawnPoint.transform);
			newPlayer.GetComponent<PlayerController> ().currentPlayer = player;
		}
	}
}