using System.IO;
using UnityEngine;

[System.Serializable]
public class Damage
{
    public enum damageType { PHYS, FIRE, COLD, BOLT, DARK }

    public float physDamage = 0;
    public float fireDamage = 0;
    public float coldDamage = 0;
    public float boltDamage = 0;
    public float darkDamage = 0;

    public Damage() { }

    public Damage(float physAmount)
    {
        physDamage = physAmount;
    }

    public Damage(float physAmount, float fireAmount, float coldAmount, float boltAmount)
    {
        physDamage = physAmount;
        fireDamage = fireAmount;
        coldDamage = coldAmount;
        boltDamage = boltAmount;
    }

    public Damage(float physAmount, float fireAmount, float coldAmount, float boltAmount, float darkAmount) : this(physAmount, fireAmount, coldAmount, boltAmount)
    {
        this.darkDamage = darkAmount;
    }

    public static Damage Multiply(Damage d1, Damage d2)
    {
        return new Damage(
            d1.physDamage * d2.physDamage,
            d1.fireDamage * d2.fireDamage,
            d1.coldDamage * d2.coldDamage,
            d1.boltDamage * d2.boltDamage,
            d1.darkDamage * d2.darkDamage
        );
    }

    public static Damage Multiply(Damage d1, float factor)
    {
        return new Damage(
            d1.physDamage * factor,
            d1.fireDamage * factor,
            d1.coldDamage * factor,
            d1.boltDamage * factor,
            d1.darkDamage * factor
        );
    }

    public static Damage Sum(Damage d1, Damage d2)
    {
        return new Damage(
            d1.physDamage + d2.physDamage,
            d1.fireDamage + d2.fireDamage,
            d1.coldDamage + d2.coldDamage,
            d1.boltDamage + d2.boltDamage,
            d1.darkDamage + d2.darkDamage
        );
    }

    public static Damage ApplyResistance(Damage d1, Resistance r)
    {
        return new Damage(
            d1.physDamage * (1 - r.physResistance / 100f),
            d1.fireDamage * (1 - r.fireResistance / 100f),
            d1.coldDamage * (1 - r.coldResistance / 100f),
            d1.boltDamage * (1 - r.boltResistance / 100f),
            d1.darkDamage * (1 - r.darkResistance / 100f)
        );
    }

    public Damage Multiply(Damage d2) { return Damage.Multiply(this, d2); }
    public Damage Multiply(float factor) { return Damage.Multiply(this, factor); }
    public Damage Sum(Damage d2) { return Damage.Sum(this, d2); }
    public Damage ApplyResistance(Resistance r) { return Damage.ApplyResistance(this, r); }

    public int CalculateRawDamage()
    {
        return (int)(physDamage + fireDamage + coldDamage + boltDamage + darkDamage);
    }
}