using System.IO;
using UnityEngine;
[System.Serializable]
public class Resistance {
    public static float MAX_RESISTANCE_VALUE = 75.0f;
    private float _physResistance = 0;
    public float physResistance {
        get { return Mathf.Min (_physResistance, MAX_RESISTANCE_VALUE); }
        set { _physResistance = Mathf.Min (value, MAX_RESISTANCE_VALUE); }
    }
    private float _fireResistance = 0;
    public float fireResistance {
        get { return Mathf.Min (_fireResistance, MAX_RESISTANCE_VALUE); }
        set { _fireResistance = Mathf.Min (value, MAX_RESISTANCE_VALUE); }
    }
    private float _coldResistance = 0;
    public float coldResistance {
        get { return Mathf.Min (_coldResistance, MAX_RESISTANCE_VALUE); }
        set { _coldResistance = Mathf.Min (value, MAX_RESISTANCE_VALUE); }
    }
    private float _boltResistance = 0;
    public float boltResistance {
        get { return Mathf.Min (_boltResistance, MAX_RESISTANCE_VALUE); }
        set { _boltResistance = Mathf.Min (value, MAX_RESISTANCE_VALUE); }
    }
    private float _darkResistance = 0;
    public float darkResistance {
        get { return Mathf.Min (_darkResistance, MAX_RESISTANCE_VALUE); }
        set { _darkResistance = Mathf.Min (value, MAX_RESISTANCE_VALUE); }
    }

    public Resistance () { }

    public Resistance (float physAmount, float fireAmount, float coldAmount, float boltAmount) {
        physResistance = physAmount;
        fireResistance = fireAmount;
        coldResistance = coldAmount;
        boltResistance = boltAmount;
    }

    public Resistance (float physAmount, float fireAmount, float coldAmount, float boltAmount, float darkAmount) : this (physAmount, fireAmount, coldAmount, boltAmount) {
        this.darkResistance = darkAmount;
    }

    public static Resistance Multiply (Resistance d1, Resistance d2) {
        return new Resistance (
            d1.physResistance * d2.physResistance,
            d1.fireResistance * d2.fireResistance,
            d1.coldResistance * d2.coldResistance,
            d1.boltResistance * d2.boltResistance,
            d1.darkResistance * d2.darkResistance
        );
    }

    public static Resistance Sum (Resistance d1, Resistance d2) {
        return new Resistance (
            d1.physResistance + d2.physResistance,
            d1.fireResistance + d2.fireResistance,
            d1.coldResistance + d2.coldResistance,
            d1.boltResistance + d2.boltResistance,
            d1.darkResistance + d2.darkResistance
        );
    }

    public Resistance Multiply (Resistance d2) { return Resistance.Multiply (this, d2); }
    public Resistance Sum (Resistance d2) { return Resistance.Sum (this, d2); }
}