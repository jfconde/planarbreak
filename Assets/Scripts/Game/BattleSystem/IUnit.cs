﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUnit {
	int GetHp ();
	int GetMaxHp ();
	Damage GetDamage ();
	Resistance GetResistance ();
	int TakeDamage (Damage d, BaseUnit source);
	int TakeDamage (int hpAmount, BaseUnit source);
	int Heal (int hpAmount);
	void Die ();
	int GetLevel ();
}
