using UnityEngine;
using System.Collections;

[System.Serializable]
public class Upgrade
{
    public string id;
    public string name;
    public string description;

    public float bonusMaxHp = 0;
    public float percentExpBoost = 0;
    public int expBoostOnKill = 0;

    public float speedBoost = 0.0f;
    public Damage flatDamageResist = new Damage();
    public Damage percentDamageResist = new Damage();
    public Damage flatDamageBoost = new Damage();
    public Damage percentDamageBoost = new Damage();

    public Upgrade(string id, string name, string description, float bonusMaxHp, float percentExpBoost, int expBoostOnKill, float speedBoost, Damage flatDamageResist, Damage percentDamageResist, Damage flatDamageBoost, Damage percentDamageBoost)
    {
        this.id = id;
        this.name = name;
        this.description = description;
        this.bonusMaxHp = bonusMaxHp;
        this.speedBoost = speedBoost;
        this.percentExpBoost = percentExpBoost;
        this.expBoostOnKill = expBoostOnKill;
        this.flatDamageResist = flatDamageResist;
        this.percentDamageResist = percentDamageResist;
        this.flatDamageBoost = flatDamageBoost;
        this.percentDamageBoost = percentDamageBoost;
    }
}