using UnityEngine.Events;
using UnityEngine;

public class BattleEventDomain : MonoBehaviour {
	// healedUnit, healAmount
	public class HealEvent: UnityEvent<BaseUnit, int> {}
	// attacked, attacker, damageAmount
	public class AttackEvent: UnityEvent<BaseUnit, BaseUnit, int> {}
	// receiver, experienceAmount
	public class GetExperienceEvent: UnityEvent<BaseUnit, int> {}
	// victim
	public class DieEvent: UnityEvent<BaseUnit> {}

	public static BattleEventDomain GetPlayerDomain () {
		GameObject eventObject = GameObject.FindGameObjectWithTag(GameTags.playerEvents);
		if (eventObject != null) {
			BattleEventDomain bed = eventObject.GetComponent<BattleEventDomain>();
			return bed;
		}
		return null;
	}

	public static BattleEventDomain GetEnemyDomain () {
		GameObject eventObject = GameObject.FindGameObjectWithTag(GameTags.enemyEvents);
		if (eventObject != null) {
			return eventObject.GetComponent<BattleEventDomain>();
		}
		return null;
	}

    public UnityEvent heal;
	public UnityEvent attack;
	public UnityEvent skill;
	public UnityEvent getExperience;

	public BattleEventDomain.AttackEvent attackEvent = new BattleEventDomain.AttackEvent();
	public BattleEventDomain.DieEvent dieEvent = new BattleEventDomain.DieEvent();

	public void fireAttackEvent (BaseUnit attacker, BaseUnit attacked, int damage) {
		this.attackEvent.Invoke(attacked, attacker, damage);
	}

	public void fireDieEvent (BaseUnit victim) {
		this.dieEvent.Invoke(victim);
	}
}
