﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseUnit : MonoBehaviour, IUnit {
	protected int hp = 10;
	protected int maxHp = 10;
	protected int level = 1;

	protected Damage damage = new Damage(1);

	protected Resistance resistance = new Resistance();
	protected BattleEventDomain playerDomain;
	protected BattleEventDomain enemyDomain;

	public void Awake () {
		TryGetEventDomains();
	}

	public void TryGetEventDomains () {
		this.playerDomain = BattleEventDomain.GetPlayerDomain();
        this.enemyDomain = BattleEventDomain.GetEnemyDomain();
	}

	public int GetLevel () {
		return this.level;
	}

	public int GetHp () {
		return this.hp;
	}

	public int GetMaxHp() {
		return this.maxHp;
	}

	public Damage GetDamage () {
		return this.damage;
	}

	public int TakeDamage (Damage d, BaseUnit source) {
		int hp = d.ApplyResistance(resistance).CalculateRawDamage();
		return this.TakeDamage(hp, source);
	}

	public virtual int TakeDamage (int hpAmount, BaseUnit source) {
		this.hp = this.hp > hpAmount ? this.hp - hpAmount : 0;
		if (this.hp == 0) {
			this.Die ();
		}
		return this.hp;
	}

	public int Heal (int hpAmount) {
		this.hp = Mathf.Max (this.hp + hpAmount, this.GetMaxHp());
		return this.hp;
	}

	public virtual void Die () {
	}

	public int GetMaxHpAtLevel (int level) {
		return level * 2;
	}

	public int GetBaseDamageAtLevel (int level) {
		return level * 2;
	}

	public Resistance GetResistance () {
		return resistance;
	}
}