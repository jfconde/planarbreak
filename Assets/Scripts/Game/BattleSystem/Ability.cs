using UnityEngine;
using System.Collections;

[System.Serializable]
public class Ability
{
    public string id;
    public string name;
    public string description;
    public bool offensive = true;

    public float cooldownInSeconds;

    public Damage.damageType type1;
    public Damage.damageType type2;

    public Ability(string id, string name, string description, bool offensive, float cooldownInSeconds, Damage.damageType type1, Damage.damageType? type2)
    {
        this.id = id;
        this.name = name;
        this.description = description;
        this.offensive = offensive;
        this.cooldownInSeconds = cooldownInSeconds;
        this.type1 = type1;
        this.type2 = type2.GetValueOrDefault();
    }
}