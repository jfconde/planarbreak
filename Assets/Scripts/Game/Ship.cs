using UnityEngine;
using System.Collections;

[System.Serializable]
public class Ship
{
    public string id;
    public string name;
    public string description;

    public float speedMultiplier;

    public float baseHp = 0.0f;
    public float hpPerLevel = 0.0f;
    public Damage baseDamage = new Damage();
    public Damage damagePerLevel = new Damage();

    public Ship(string id, string name, string description, float baseHp, float hpPerLevel, float speedMultiplier, Damage baseDamage, Damage damagePerLevel)
    {
        this.id = id;
        this.name = name;
        this.description = description;
        this.baseHp = baseHp;
        this.hpPerLevel = hpPerLevel;
        this.speedMultiplier = speedMultiplier;
        this.baseDamage = baseDamage;
        this.damagePerLevel = damagePerLevel;
    }
}