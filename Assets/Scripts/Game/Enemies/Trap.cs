using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : MonoBehaviour {
    public Damage damage = new Damage(1,0,0,0,0);

    public void Awake () {
    }

    public void OnCollisionEnter2D (Collision2D collision) {
        if (collision.gameObject.tag == "player") {
            PlayerController playerController = collision.gameObject.GetComponent<PlayerController> ();
            if (playerController.currentPlayer != null) {
                this.OnPlayerCollision(playerController.currentPlayer, collision.contacts);    
            }
        }
    }

    protected void OnPlayerCollision (Player player, ContactPoint2D[] contacts) {
        player.TakeDamage(this.damage, null);
    }
}