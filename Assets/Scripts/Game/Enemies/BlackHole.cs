﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CircleCollider2D))]
public class BlackHole : MonoBehaviour {
    [SerializeField]
    private LayerMask m_Mask = -1;
    [SerializeField]
    private float m_Force = 500f;

    private void OnValidate()
    {
        GetComponent<CircleCollider2D>().isTrigger = true;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        Debug.Log("Mask: " + m_Mask.value + ", Collision Layer: " + (1<<collision.gameObject.layer)+" , Bitwised:"+ ((1 << collision.gameObject.layer) & m_Mask.value));

        if ( ((1<<collision.gameObject.layer) & m_Mask.value) > 0 )
        {
            Vector2 force = transform.position - collision.transform.position;
            float distance = Mathf.Abs(force.magnitude);

            float colliderRadius = GetComponent<CircleCollider2D>().radius;
            float multiplier = (colliderRadius / distance);
            Debug.Log("Multiplier:" + multiplier);
            force = force.normalized * multiplier * m_Force;
            Debug.Log("Force:" + force);
            collision.attachedRigidbody.AddForce(force.normalized * multiplier * m_Force);
        }
    }
}
