public static class GameTags {
    public static string player = "player";
    public static string level = "level";
    public static string playerEvents = "playerEvents";
    public static string enemyEvents = "enemyEvents";
    public static string gameData = "gamedata";

}