﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using UnityEngine;

/**
 * v1.0
 * @author jfconde
 * @class PlayerData
 * @description Player data we find relevant enough to be persisted.
 */
[Serializable]
public class PlayerData {
	public const string glyphs = "abcdefghijklmnopqrstuvwxyz0123456789";
	public const int minIdLength = 8;
	public const int maxIdLength = 16;
	public string id { get; }
	public int level = 1;
	public int experience = 0;

	public long money = 0;
	public List<string> unlockedLevels;
	public List<Ship> unlockedShips;
	public List<Ability> unlockedAbilities;
	public List<Upgrade> unlockedUpgrades;

	public int currentShip = 0;
	
	public PlayerData () {
		this.id = GenerateRandomKey (PlayerData.minIdLength, PlayerData.maxIdLength);
		this.unlockedLevels = new List<string> ();
		this.unlockedShips = new List<Ship> ();
	}

	public PlayerData (string id) {
		this.id = id;
		this.unlockedLevels = new List<string> ();
		this.unlockedShips = new List<Ship> ();
	}

	public static string GenerateRandomKey (int minLength, int maxLength) {
		System.Random random = new System.Random ();
		StringBuilder result = new StringBuilder ("");
		int charAmount = random.Next (minLength, maxLength);
		for (int i = 0; i < charAmount; i++) {
			result.Append (PlayerData.glyphs[random.Next (0, PlayerData.glyphs.Length)]);
		}

		return result.ToString ();
	}

	public void Save () {
		GameSaveManager.GetInstance().SaveGame(this);
	}
}