﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

/**
 * v1.0
 * @author jfconde
 * @class PlayerData
 * @description Utility class to load player saves.
 */
[Serializable] 
public class GameSaveManager
{
	[NonSerialized]
	public static GameSaveManager instance;	
	public static string SaveFileExtension = ".dat";
	public static string defaultPlayerFileName = "player";
	public List<PlayerData> playerSaves = new List<PlayerData>();

	public GameSaveManager() {
		GameSaveManager.instance = this;
	}

	public static GameSaveManager GetInstance () {
		if (Obj.NotNull(instance)) {
			return instance;
		}else{
			return new GameSaveManager();
		}
	}

	public void SaveGame (PlayerData save)
	{
		FileStream fileStream = new FileStream (Application.persistentDataPath + "/" + GameSaveManager.defaultPlayerFileName + GameSaveManager.SaveFileExtension, 
			                        FileMode.OpenOrCreate, FileAccess.Write);
		BinaryFormatter serializer = new BinaryFormatter ();
		serializer.Serialize (fileStream, save);
		fileStream.Close();
	}

	public PlayerData LoadGame (string filePath)
	{
		if (File.Exists (filePath)) {
		
			FileStream fileStream = new FileStream (filePath, 
				                        FileMode.Open, FileAccess.Read);
			BinaryFormatter serializer = new BinaryFormatter ();
			PlayerData save = ((PlayerData)serializer.Deserialize (fileStream));
			fileStream.Close();
			return save;
		} else {
			return null;
		}
	}

	public List<PlayerData> LoadGameSaves () {
		FileInfo[] files = this.LoadGameSaveList ();
		List<PlayerData> newPlayerSaves = new List<PlayerData> ();
		foreach(FileInfo file in files) {
			PlayerData saveData = this.LoadGame (file.FullName);
			if (saveData != null) {
				newPlayerSaves.Add (saveData);
			}
		}
		this.playerSaves = newPlayerSaves;
		return newPlayerSaves;
	}

	public FileInfo[] LoadGameSaveList ()
	{
		DirectoryInfo dir = new DirectoryInfo (Application.persistentDataPath);
		FileInfo[] files = dir.GetFiles ("player" + GameSaveManager.SaveFileExtension);
		return files;
	}
}
