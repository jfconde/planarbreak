﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : BaseUnit
{
    private PlayerData playerData;
    private float speed = 0.0f;
    public int experience = 0;
    private Object deadCanvas;
    private Ship ship;

    public string id = "";
    // Use this for initialization
    void Start()
    {
        playerDomain = BattleEventDomain.GetPlayerDomain();
        enemyDomain = BattleEventDomain.GetEnemyDomain();
        deadCanvas = Resources.Load("MenuDead/CanvasPrefab", typeof(GameObject));
    }

    // Update is called once per frame
    void Update() { }

    void Destroy()
    {
        ship = null;
    }

    public BattleEventDomain GetPlayerDomain()
    {
        if (playerDomain)
        {
            return playerDomain;
        }
        else
        {
            return playerDomain = BattleEventDomain.GetPlayerDomain();
        }
    }

    public BattleEventDomain GetEnemyDomain () {
          if (enemyDomain)
        {
            return enemyDomain;
        }
        else
        {
            return enemyDomain = BattleEventDomain.GetEnemyDomain();
        }
    }

    public static Player GetCurrentPlayer()
    {
        GameObject player = GameObject.FindGameObjectWithTag(GameTags.player);
        if (player != null)
        {
            return player.GetComponent<Player>();
        }
        return null;
    }

    public void SetSpeed(float speed)
    {
        this.speed = speed;
    }

    public int GetExperience()
    {
        return this.experience;
    }

    public string GetId()
    {
        return this.id;
    }

    public void SetPlayerData(PlayerData playerData)
    {
        this.experience = playerData.experience;
        this.id = playerData.id;
        this.playerData = playerData;
        this.RespawnValues();
    }

    public void RespawnValues()
    {
        this.hp = this.maxHp = this.GetMaxHpAtLevel(this.level);
    }

    public override int TakeDamage(int hpAmount, BaseUnit source)
    {
        this.hp = this.hp > hpAmount ? this.hp - hpAmount : 0;

        GetPlayerDomain().fireAttackEvent(source, this, hpAmount);

        if (this.hp == 0)
        {
            this.Die();
            GetPlayerDomain().fireDieEvent(this);
        }

        return this.hp;
    }

    public override void Die()
    {
        GameObject levelObject = GameObject.FindGameObjectWithTag("level");
        Level level = levelObject != null ? levelObject.GetComponent<Level>() : null;

        if (level != null)
        {
            Time.timeScale = 0f;
            GameObject instance = Instantiate(deadCanvas) as GameObject;
        }
    }
    public void addLevelAccess(string levelName)
    {
        if (!this.playerData.unlockedLevels.Contains(levelName))
        {
            this.playerData.unlockedLevels.Add(levelName);
        }
    }

    public void SetShip(Ship ship)
    {
        // oh yeah!
        this.ship = ship;
        this.SetAttributesFromShip();
    }

    public void SetAttributesFromShip()
    {
        hp = maxHp = (int)(this.GetMaxHpAtLevel(level) + ship.baseHp + (ship.hpPerLevel * level));
        this.damage = ship.baseDamage.Sum(ship.damagePerLevel.Multiply(level - 1));
    }
}