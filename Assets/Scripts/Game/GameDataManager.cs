﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameDataManager : MonoBehaviour {
	public PlayerData playerData;
	public Player playerObj;

	public string nextSpawnPoint;

	public static GameDataManager GetCurrentManager () {
		GameObject gameData = GameObject.FindGameObjectWithTag(GameTags.gameData);
		if (gameData != null) {
			return gameData.GetComponent<GameDataManager>();	
		}
		return null;
	}

	public static GameObject GetCurrentGameObject () {
		return GameObject.FindGameObjectWithTag(GameTags.gameData);
	}

	public void Start () {
		GameObject.DontDestroyOnLoad (this.gameObject);
		this.playerObj = gameObject.GetComponent <Player> ();

		// For debugging purposes
		if (playerData != null) {
			this.SetPlayer (playerData);
		}
	}

	void Awake()
	{
		if (this.playerObj) {
			this.playerObj.RespawnValues();
		}
	}

	public void SetPlayer (PlayerData playerData) {
		this.playerData = playerData;
		this.playerObj.SetPlayerData (playerData);
	}
}
