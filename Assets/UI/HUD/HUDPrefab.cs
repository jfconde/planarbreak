﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDPrefab : MonoBehaviour {
	public Slider hpSlider;
	public Text levelText;
	Player player;

	void Awake () {
		TryGetPlayer ();
	}

	void TryGetPlayer () {
		GameObject gameData = GameObject.FindGameObjectWithTag ("gamedata");
		if (gameData != null) {
			player = gameData.GetComponent<GameDataManager> ().playerObj;
		}
	}

	void Update () {
		if (player == null) {
			TryGetPlayer ();
		} else {
			if (hpSlider != null) {
				hpSlider.value = player.GetMaxHp() > 0 ? ((float)player.GetHp() / (float)player.GetMaxHp()) : 0;
			}
			if (levelText != null) {
				levelText.text = player.GetLevel().ToString();
			}
		}
	}
}
