﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
/**
 * v1.0
 * @author jfconde
 * @class MainMenuScript
 * @description Controls the Main Menu, its initalization and transitions.
 */
public class MainMenuScript : MonoBehaviour {
	private GameSaveManager gameSaveManager;
	public Button loadGameButton;
	public Button createGameButton;
	public GameObject gameDataPrefab;
	public GameObject mainCanvas;

	public string playerName;

	void Start () {
		this.ClearManagers ();
		this.LoadSavedData ();
	}

	void Update () {

	}

	void LoadSavedData () {
		gameSaveManager = GameSaveManager.GetInstance ();
		gameSaveManager.LoadGameSaves ();

		if (loadGameButton != null) {
			loadGameButton.interactable = (gameSaveManager.playerSaves.Count > 0);
		}
	}

	void ShowMainMenu () { }

	public void ContinueGame () {
		if (gameSaveManager.playerSaves.Count > 0) {
			this.ClearManagers ();
			GameObject gameDataObject = GameObject.Instantiate (gameDataPrefab);
			GameDataManager manager = gameDataObject.GetComponent<GameDataManager> ();
			manager.Start ();
			manager.SetPlayer (gameSaveManager.playerSaves[0]);
			SceneManager.LoadScene ("MainMap");
		}
	}

	public int ClearManagers () {
		GameObject[] gameDataObjects = GameObject.FindGameObjectsWithTag ("gamedata");
		foreach (GameObject gameDataObject in gameDataObjects) {
			GameObject.Destroy (gameDataObject);
		}
		return gameDataObjects.Length;
	}

	public void CreateNewGame () {
		if (!IsPlayerNameValid (playerName)) {
			return;
		}
		if (gameSaveManager.playerSaves.Count > 0) {
			Msg.Confirm (
				mainCanvas,
				"WARNING",
				"Your current game will be erased. Do you want to continue?",
				(bool confirm) => {
					if (confirm)
						DoCreateNewGame ();
				}
			);
		} else {
			DoCreateNewGame ();
		}

	}

	public void DoCreateNewGame () {
		PlayerData newPlayer = new PlayerData (playerName);
		this.ClearManagers ();
		GameObject gameDataObject = GameDataManager.GetCurrentGameObject ();
		if (gameDataObject == null) {
			gameDataObject = GameObject.Instantiate (gameDataPrefab);
			gameDataObject.GetComponent<GameDataManager> ().Start ();
		}
		newPlayer.Save ();
		gameDataObject.GetComponent<GameDataManager> ().SetPlayer (newPlayer);
		SceneManager.LoadScene ("MainMap");
	}

	public void OnPlayerNameTextFieldChange (InputField input) {
		playerName = input.text;
		if (createGameButton != null) {
			createGameButton.interactable = IsPlayerNameValid (playerName);
		}
	}

	public bool IsPlayerNameValid (string name) {
		Regex re = new Regex (@"[a-zA-Z0-9]*");
		return name.Length > 2 && name.Length < 16 && re.IsMatch (name);
	}
}