﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class MainMapScript : MonoBehaviour
{
    private static string levelPrefix = "Scenes/Levels/";
    private static string menuPath = "Scenes/MainMenu/MainMenu";
    public Text playerName;
    public Text playerLevel;
    public Text playerHp;
    public GameObject levelGroupList;
    public GameObject levelItemGrid;

    public GameObject levelGroupPrefab;
    public GameObject levelItemPrefab;

    public LevelManager levelManager;

    // Use this for initialization
    void Start()
    {
        levelManager = new LevelManager();
        UpdateUI();
        UpdateLevelGroupList();
    }

    void UpdateLevelGroupList () {
        GameDataManager manager = GameDataManager.GetCurrentManager();
        List<string> unlockedLevels;

        if (manager != null && manager.playerData != null)
        {
            unlockedLevels = GameDataManager.GetCurrentManager().playerData.unlockedLevels;
        }
        else
        {
            unlockedLevels =null;
        }
        SetUnlockedLevels(unlockedLevels);
    }

    void UpdateUI()
    {
        GameDataManager manager = GameDataManager.GetCurrentManager();
        if (manager != null)
        {
            Player player = manager.playerObj;
            if (playerName != null)
                playerName.text = player.GetId();
            if (playerLevel != null)
                playerLevel.text = player.GetLevel().ToString();
            if (playerHp != null)
                playerHp.text = player.GetMaxHp().ToString();

        }
        else
        {
            Debug.Log("No object with tag gamedata could be found.");
        }
    }

    public void LoadMapInformation () {

    }

    // Update is called once per frame
    void Update()
    {

    }

	public void SetUnlockedLevels (List<string> levelList) {
		GameObject newObj;

        if (levelGroupList == null) {
            return;
        }

        LevelGroup[] groups = levelManager.GetLevelGroups();
		for (int i = 0; i < groups.Length; i++)
		{
            LevelGroup group = groups[i];
			// Create new instances of our prefab until we've created as many as we specified
			newObj = (GameObject)Instantiate(levelGroupPrefab, levelGroupList.transform);
            // Update the button controller
            LevelGroupButtonController buttonController = newObj.GetComponent<LevelGroupButtonController>();
            if (buttonController != null) {
                buttonController.SetLevelGroup(group);
                buttonController.GetButton().onClick.AddListener(() => OnLevelGroupItemClick(group));
            }
		}
	}

    public void OnLevelGroupItemClick (LevelGroup group) {
        LoadLevelItemsGrid(group);
    }

    public void OnLevelItemClick (LevelItem item) {
        SceneManager.LoadScene(MainMapScript.levelPrefix + item.id);
    }

    public void GoToMapSelector () {
        levelGroupList.transform.parent.parent.gameObject.SetActive(true);
        levelItemGrid.transform.parent.parent.gameObject.SetActive(false);
    }

    public void LoadLevelItemsGrid (LevelGroup group) {
        levelItemGrid.transform.parent.parent.gameObject.SetActive(true);
        LevelItem [] items = group.levels;
        // Destroy previous items
        foreach (Transform child in levelItemGrid.transform) {
            GameObject.Destroy(child.gameObject);
        }
        // Create new items
        GameObject newObj;
        foreach (LevelItem item in items) {
            newObj = (GameObject)Instantiate(levelItemPrefab, levelItemGrid.transform);
            LevelItemButtonController buttonController = newObj.GetComponent<LevelItemButtonController>();
            if (buttonController != null) {
                buttonController.SetLevelItem(item);
                buttonController.GetButton().onClick.AddListener(() => OnLevelItemClick(item));
            }
        }
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene(MainMapScript.menuPath);
    }

    public void LoadLevel(string mapName)
    {
        SceneManager.LoadScene(MainMapScript.levelPrefix + mapName);
    }
}
