﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelItemButtonController : MonoBehaviour {
    public Text groupNameText;
    // Use this for initialization
    public LevelItem LevelItem;

    public static string defaultName = "N/A";

    void Start () {
        UpdateElements ();
    }

    void UpdateElements () {
        if (LevelItem != null) {
            groupNameText.text = LevelItem.name;
        } else {
            groupNameText.text = LevelItemButtonController.defaultName;
        }
    }

    // Update is called once per frame
    void Update () {

    }

    public Button GetButton () {
        return this.GetComponent<Button> ();
    }

    public void SetLevelItem (LevelItem item) {
        this.LevelItem = item;
        this.GetButton ().interactable = item.unlocked || GameDataManager.GetCurrentManager ().playerData.unlockedLevels.Contains (item.id);
    }
}