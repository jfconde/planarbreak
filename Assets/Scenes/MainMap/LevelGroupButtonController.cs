﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class LevelGroupButtonController : MonoBehaviour
{
    public Text groupNameText;
    public Image buttonImage;
    // Use this for initialization
    public LevelGroup levelGroup;

    public static string defaultName = "N/A";
    public static Color defaultColor = Color.white;

    void Start()
    {
        UpdateElements();
    }

    void UpdateElements()
    {
        if (levelGroup != null)
        {
            Color parsedButtonColor;
            ColorUtility.TryParseHtmlString(levelGroup.color, out parsedButtonColor);
            groupNameText.text = levelGroup.name;
            buttonImage.color = parsedButtonColor != null ? parsedButtonColor : LevelGroupButtonController.defaultColor;
        }
        else
        {
			groupNameText.text = LevelGroupButtonController.defaultName;
			buttonImage.color = LevelGroupButtonController.defaultColor;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

	public Button GetButton () {
		return this.GetComponent<Button>();
	}

    public void SetLevelGroup(LevelGroup group)
    {
        this.levelGroup = group;
		
    }
}
