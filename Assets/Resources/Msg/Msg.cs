﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Msg : MonoBehaviour {
    public static void Confirm (GameObject canvas, string title, string text, Action<bool> callback) {
        GameObject Confirm = Resources.Load ("Msg/Confirm") as GameObject;
        GameObject msg = Instantiate (Confirm, canvas.transform);
        ConfirmScript script = msg.GetComponent<ConfirmScript> ();
        script.Show (title, text, callback);
    }
}