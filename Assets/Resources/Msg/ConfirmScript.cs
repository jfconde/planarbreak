﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConfirmScript : MonoBehaviour
{
    public Button yesButton;
    public Button noButton;
    public Text title;
    public Text content;

    public void Show (string titleText, string contentText, Action<bool> callback) {
        title.text = titleText;
        content.text = contentText;
        yesButton.onClick.AddListener(() => Hide(true, callback));
        noButton.onClick.AddListener(() => Hide(false, callback));
        GetComponent<Animator>().Play("ConfirmShow");
    }

    private void Hide (bool result, Action<bool> callback) {
        callback(result);
        GameObject.Destroy(this.gameObject);
    }
}
