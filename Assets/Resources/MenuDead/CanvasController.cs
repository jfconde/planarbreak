﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasController : MonoBehaviour {
	public void OnRestart () {
		Level level = GameObject.FindGameObjectWithTag(GameTags.level).GetComponent<Level>();
		level.Reload();
	}

	public void OnQuit () {
		Level level = GameObject.FindGameObjectWithTag(GameTags.level).GetComponent<Level>();
		level.Quit();
	}
}
